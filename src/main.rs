mod execute;
mod lexer;
mod parser;
mod print;
mod unrecoverable_error;
//
use std::env;
use std::process::exit;

fn main() {
    let args: Vec<String> = env::args().collect();
    match args.len() as u32 {
        1 => {
            print!(
                "\x1b[93mempl:\x1b[0m Invalid option ! 'no arguments provided' \n
                        Options: \n
                        <script_file> \t Runs the file provides
                        --i \t\t\t Runs empl in interactive mode
 "
            );
            exit(64);
        }
        2 => match args[1].as_str() {
            "--i" => {
                execute::run_repl();
            }
            _ => {
                execute::file_run(&args[1]);
            }
        },
        _ => println!("Usage: empl [script]"),
    }
}
