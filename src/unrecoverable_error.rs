use std::process;

pub fn report_token(line: i32, msg: &str, index: i32) {
    eprintln!("Error at line {} index {} at index {}", line, msg, index);
    process::exit(64);
}
