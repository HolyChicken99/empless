use crate::lexer;
use crate::parser;
use crate::unrecoverable_error;

use std::io::{self, Write};

pub fn file_run(val: &String) {
    lexer::lexing(val);
    parser::pol();
    // let data = std::fs::read_to_string(&val).expect("unable to read file");
    // println!("{}", data);
    // unrecoverable_error::report(1, &String::from("pol"));
}

pub fn run_repl() {
    loop {
        print!("empl> ");
        io::stdout().flush().unwrap();
        let mut string = String::new();
        io::stdin().read_line(&mut string).expect("err");
    }
}
