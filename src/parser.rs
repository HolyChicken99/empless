// use crate::print;

// > 1 + 2 - 3 :multple binary operations
// > "String is pol": string
// > -(1+2):  unary operation with a binary
// > 1: single number statement

use crate::lexer;
#[derive(Debug)]
pub enum NodeType {
    Number(i32),
    BinaryOp,
    String,
    Unary,
    Root,
}

#[derive(Debug)]
pub struct ASTNode {
    //NODE TYPE
    pub nodeT: NodeType,

    // Binary
    pub left: Option<Box<ASTNode>>,
    pub right: Option<Box<ASTNode>>,
    pub operatorB: Option<u8>,

    //unary
    pub operatorU: Option<u8>,
    pub valU: Option<i32>,

    //leaf int
    pub valI: Option<i32>,

    //leaf string
    pub string: Option<String>,

    //Root ast Node
    // Doing a single leave right now i.e. single expression
    pub leave: Option<Box<ASTNode>>,
}

pub fn print_expression(ast: &ASTNode) {
    println!("{:?}", *ast);
}

fn parse_expression(tokens: Vec<lexer::token>) -> ASTNode {
    let mut ast = ASTNode {
        nodeT: NodeType::Root,
        left: None,
        right: None,
        operatorB: None,
        operatorU: None,
        valU: None,
        valI: None,
        string: None,
        leave: None,
    };
    let mut tokens = tokens.iter();
    let val = tokens.next().unwrap();
    match val.tok {
        lexer::token_type::STRING => ASTNode {
            leave: Some(Box::new(ASTNode {
                nodeT: NodeType::String,
                left: None,
                right: None,
                operatorB: None,
                operatorU: None,
                valU: None,
                valI: None,
                string: Some(val.lexeme.clone()),
                leave: None,
            })),
            ..ast
        },
        // lexer::token_type::NUMBER => {ast}
        _ => ast,
    }
    // print!("val is {}", val.lexeme);
}

pub fn pol() {
    let mut tokens = Vec::<lexer::token>::new();
    tokens.push(lexer::token {
        tok: lexer::token_type::STRING,
        lexeme: "higher".to_string(),
        line: 1,
    });

    let pt = parse_expression(tokens);
    print_expression(&pt);
}

//   while let Some(token) = tokens.next() {
//         println!("ast rep is {:?}", ast);
//         let operator = token.chars().next().unwrap();
//         println!("the operator is {}", operator);
//         let right = parse_term(tokens.next().unwrap())?;
//         println!("right representation is {:?}", right);

//         ast = AstNode::BinaryOp {
//             operator,
//             left: Box::new(ast),
//             right: Box::new(right),
//         };
//     }
//     Ok(ast)
// }
