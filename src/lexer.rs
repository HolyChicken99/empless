use crate::unrecoverable_error;
use std::collections::HashSet;

#[allow(non_camel_case_types)]
#[derive(Debug)]
pub enum token_type {
    // Single-character tokens.
    LEFT_PAREN,
    RIGHT_PAREN,
    LEFT_BRACE,
    RIGHT_BRACE,
    COMMA,
    DOT,
    MINUS,
    PLUS,
    SEMICOLON,
    SLASH,
    STAR,
    // One or two character tokens.
    BANG,
    BANG_EQUAL,
    EQUAL,
    EQUAL_EQUAL,
    GREATER,
    GREATER_EQUAL,
    LESS,
    LESS_EQUAL,
    // Literals.
    IDENTIFIER,
    STRING,
    NUMBER,
    // Keywords.
    AND,
    CLASS,
    ELSE,
    FALSE,
    FUN,
    FOR,
    IF,
    NIL,
    OR,
    PRINT,
    RETURN,
    SUPER,
    THIS,
    TRUE,
    VAR,
    WHILE,
    EOF,
}

#[allow(non_camel_case_types)]
pub struct token {
    pub tok: token_type,
    pub lexeme: String,
    pub line: i32,
}
fn is_at_end(curr: i32, size: i32) -> bool {
    if curr != size {
        return false;
    } else {
        return true;
    };
}
fn find_char(curr: i32, char_: u8, src: &str) -> i32 {
    // to find the index of the occurrence of char_ in the line
    let mut i = curr;
    i += 1;
    while !is_at_end(i, src.len() as i32) {
        if src.as_bytes()[i as usize] == char_ {
            return i as i32;
        }
        i += 1;
    }
    return -1;
}

///NOTE Functions to help parse numeric sequences
fn peek_char(src: &str, curr: i32) -> i8 {
    // peeks 1 char ahead
    if curr + 1 < src.len() as i32 {
        let var = src.as_bytes()[(curr + 1) as usize];
        return var as i8;
    }
    -1
}

fn is_digit(char_: u8) -> bool {
    if char_ >= 48 && char_ <= 57 {
        return true;
    }
    return false;
}

fn parse_number(src: &str, curr: i32) -> Option<String> {
    let mut var = curr;
    let mut string = String::new();
    while var < (src.len()) as i32 {
        if is_digit(src.as_bytes()[var as usize]) {
            string.push(src.as_bytes()[var as usize] as char);
        } else if src.as_bytes()[var as usize] == b'.' {
            if is_digit(peek_char(src, var) as u8) {
                string.push('.');
            } else {
                return None;
            }
        } else if src.as_bytes()[var as usize] == b' ' || src.as_bytes()[var as usize] == b'\n' {
            return Some(string);
        } else {
            return None;
        }
        var += 1;
    }

    print!("found {}", var);
    // print!("value found is {}", string.parse::<f64>().unwrap());
    Some(string) // return None;
}

///NOTE Functions to help with reserved
fn is_alpha(char_: u8) -> bool {
    if char_ >= 97 && char_ <= 122 {
        return true;
    }
    return false;
}

fn identifier(src: &str, curr: i32, line: i32) -> Option<token> {
    // verify if a reserved keyword is found
    let mut set: HashSet<String> = HashSet::new();
    set.insert("and".to_string());
    set.insert("or".to_string());
    set.insert("if".to_string());
    set.insert("else".to_string());
    set.insert("false".to_string());
    set.insert("true".to_string());
    set.insert("nil".to_string());
    set.insert("for".to_string());
    set.insert("print".to_string());
    set.insert("while".to_string());
    // set.insert("and".to_string());
    set.insert("var".to_string());
    set.insert("fun".to_string());
    let mut i = curr;
    println!("curr is {}", curr);
    while i < src.len() as i32 && src.as_bytes()[i as usize] != b' ' {
        i += 1;
        println!("i is {}", i);
    }

    let substring = &src[(curr as usize)..(i as usize)];
    if (set.contains(substring)) {
        match substring {
            "and" => {
                return Some(token {
                    tok: token_type::AND,
                    lexeme: "and".to_string(),
                    line: line,
                })
            }
            "or" => {
                return Some(token {
                    tok: token_type::OR,
                    lexeme: "or".to_string(),
                    line: line,
                })
            }
            "if" => {
                return Some(token {
                    tok: token_type::IF,
                    lexeme: "if".to_string(),
                    line: line,
                })
            }
            "else" => {
                return Some(token {
                    tok: token_type::OR,
                    lexeme: "or".to_string(),
                    line: line,
                })
            }
            "false" => {
                return Some(token {
                    tok: token_type::FALSE,
                    lexeme: "false".to_string(),
                    line: line,
                })
            }
            "true" => {
                return Some(token {
                    tok: token_type::TRUE,
                    lexeme: "true".to_string(),
                    line: line,
                })
            }
            "nil" => {
                return Some(token {
                    tok: token_type::NIL,
                    lexeme: "nil".to_string(),
                    line: line,
                })
            }
            "for" => {
                return Some(token {
                    tok: token_type::FOR,
                    lexeme: "for".to_string(),
                    line: line,
                })
            }
            "print" => {
                return Some(token {
                    tok: token_type::PRINT,
                    lexeme: "print".to_string(),
                    line: line,
                })
            }
            "while" => {
                return Some(token {
                    tok: token_type::WHILE,
                    lexeme: "while".to_string(),
                    line: line,
                })
            }
            "var" => {
                return Some(token {
                    tok: token_type::VAR,
                    lexeme: "var".to_string(),
                    line: line,
                })
            }
            "fun" => {
                return Some(token {
                    tok: token_type::FUN,
                    lexeme: "fun".to_string(),
                    line: line,
                })
            }
            _ => {
                return None;
            }
        }
    } else {
        return None;
    }
}

///NOTE  Functions for special sequences that should be treated as a single token
fn match_char(src: &str, char_: u8, cursor: i32) -> bool {
    // match_char peeks 1 index ahea
    let len = src.len() as i32;
    // we want to stop 1 before the end
    if cursor <= len - 2 {
        let data = src.as_bytes();
        let val = (cursor + 1) as usize;
        if data[val] == char_ {
            return true;
        }
    }
    return false;
}

#[allow(non_snake_case)]
///NOTE Main method for reading file and splitting into tokens
// #[allow(non_snake_case)]
pub fn lexing(path: &String) {
    let mut line: i32 = 1;
    let dataS = std::fs::read_to_string(&path).expect("Unable to read file");
    let data = dataS.as_str();
    dbg!(data);
    let mut tokens: Vec<token> = Vec::<token>::new();
    let mut i = 0;
    while i < data.len() {
        match data.as_bytes()[i] {
            b'(' => tokens.push(token {
                tok: token_type::LEFT_PAREN,
                lexeme: "(".to_string(),
                line: line,
            }),
            b')' => tokens.push(token {
                tok: token_type::RIGHT_PAREN,
                lexeme: ")".to_string(),
                line: line,
            }),
            b'{' => tokens.push(token {
                tok: token_type::LEFT_BRACE,
                lexeme: "{".to_string(),
                line: line,
            }),
            b'}' => tokens.push(token {
                tok: token_type::RIGHT_BRACE,
                lexeme: "}".to_string(),
                line: line,
            }),
            b',' => tokens.push(token {
                tok: token_type::COMMA,
                lexeme: ",".to_string(),
                line: line,
            }),
            b'.' => tokens.push(token {
                tok: token_type::DOT,
                lexeme: ",".to_string(),
                line: line,
            }),
            b'-' => tokens.push(token {
                tok: token_type::MINUS,
                lexeme: "-".to_string(),
                line: line,
            }),
            b'+' => tokens.push(token {
                tok: token_type::PLUS,
                lexeme: "+".to_string(),
                line: line,
            }),
            b';' => tokens.push(token {
                tok: token_type::SEMICOLON,
                lexeme: ";".to_string(),
                line: line,
            }),
            b'*' => tokens.push(token {
                tok: token_type::SEMICOLON,
                lexeme: "*".to_string(),
                line: line,
            }),
            b'!' => {
                if match_char(data, b'=', i as i32) {
                    i += 1;
                    tokens.push(token {
                        tok: token_type::BANG_EQUAL,
                        lexeme: "!=".to_string(),
                        line: line,
                    })
                } else {
                    tokens.push(token {
                        tok: token_type::BANG,
                        lexeme: "!".to_string(),
                        line: line,
                    })
                }
            }
            b'/' => {
                // println!("trying to match '/ at index {}", i);
                if match_char(data, b'/', i as i32) {
                    let len_end: i32 = find_char(i as i32, b'\n', data);
                    println!("newline found at len_end {}", len_end);
                    i += len_end as usize;
                }
            }
            b'\n' => {
                line += 1;
            }
            b' ' | b'\t' | b'\r' => {}

            //TODO : add newline error in string detection
            b'"' => {
                let cor_br = find_char(i as i32, b'"', data);
                if cor_br != -1 {
                    println!("cor_br : {} , i : {}", cor_br, i);
                    let substr = &data[i + 1..(cor_br as usize)];
                    println!("the text is : {}, of len {}", substr, substr.len());
                    i += substr.len() + 2;
                    tokens.push(token {
                        tok: token_type::STRING,
                        lexeme: substr.to_string(),
                        line: line,
                    })
                } else {
                    unrecoverable_error::report_token(line, "Malformed String", i as i32);
                }
            }
            _ => {
                if is_digit(data.as_bytes()[i]) {
                    println!("number found at index i {}", i);
                    let var = parse_number(data, i as i32);
                    match var {
                        Some(x) => {
                            let len = x.len();
                            i += len;
                            tokens.push(token {
                                tok: token_type::NUMBER,
                                lexeme: x,
                                line: line,
                            })
                        }
                        None => {
                            unrecoverable_error::report_token(line, "Malformed Number ", i as i32)
                        }
                    }
                } else if is_alpha(data.as_bytes()[i]) {
                    // checks for reserved words
                    let var = identifier(data, i as i32, line);
                    match var {
                        Some(x) => {
                            let lex = &x.lexeme;
                            i += lex.len();
                            tokens.push(x);
                        }
                        None => print!("none"),
                    }
                } else {
                    unrecoverable_error::report_token(line, "Wrong token found", i as i32);
                }
            }
        }
        i += 1; // increments the while loops counter
    }
    println!("parsing succesful final string ");
    for i in tokens {
        println!("{:?},{:?},{:?}", i.lexeme, i.line, i.tok);
    }
}
